﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    #region Structure and class

    /**
    * \struct Coordinates
    * \brief coordinates
    */
    struct Coordinates
    {
        public int tileX, tileY;

        public Coordinates(int tileX, int tileY)
        {
            this.tileX = tileX;
            this.tileY = tileY;
        }
    }


    /**
    * \class Room
    */
    class Room
    {
        public List<Coordinates> tilesList;
        public List<Coordinates> edgeTilesList;
        public List<Room> connectedRooms; //connected 2 room that share a common passage
        public int roomSize;


        public Room()
        {
        }

        public Room(List<Coordinates> roomTilesList, int[,] map)
        {
            print("map.Length : " + map.Length);

            tilesList = roomTilesList;
            roomSize = tilesList.Count;
            connectedRooms = new List<Room>();

            edgeTilesList = new List<Coordinates>();
            foreach (Coordinates tileCoordinates in tilesList)
            {
                print("tileCoordinates : " + tileCoordinates.tileX + " | " + tileCoordinates.tileY);
                for (int x = tileCoordinates.tileX - 1; x <= tileCoordinates.tileX + 1; x++)
                {
                    for (int y = tileCoordinates.tileY - 1; y <= tileCoordinates.tileY + 1; y++)
                    {
                        print("x=" + x + " | y=" + y);
                        if (x == tileCoordinates.tileX || y == tileCoordinates.tileY)
                        {
                            /*if (map[x, y] == 1)
                            {
                                edgeTilesList.Add(tileCoordinates);
                            }*/
                        }
                    }
                }
            }
        }


        public static void ConnectRooms(Room roomA, Room roomB)
        {
            roomA.connectedRooms.Add(roomB);
            roomB.connectedRooms.Add(roomA);
        }


        public bool IsConnected(Room otherRoom)
        {
            return connectedRooms.Contains(otherRoom);
        }
    }

    #endregion


    //___________________________________________________
    //___________________________________________________

    #region Attributes

    public int width, height, borderSize;
    public int wallThresholdSize, roomThresholdSize;

    [Range(0, 100)] public int randomFillPercent;

    public string seed;
    public bool useRandomSeed;


    private int[,] map;

    #endregion

    //_____________________________________________
    //_____________________________________________

    #region Main functions

    void RandomFillMap()
    {
        if (useRandomSeed) seed = Time.time.ToString();
        System.Random prng = new System.Random(seed.GetHashCode()); //getHashCode convertit le string en integer

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                // Ensure that all rooms are enclosed in the map area
                if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                {
                    map[x, y] = 1;
                }
                else
                {
                    //Using the if/else notation, the current index will be set to 1 if the generated number is less than fillPercentage,
                    //otherwise it will be set to 0
                    map[x, y] = (prng.Next(0, 100) < randomFillPercent) ? 1 : 0;
                }
            }
        }
    }


    bool IsInMapRange(int x, int y)
    {
        return (x >= 0 && x < width && y >= 0 && y < height);
    }


    int GetSurroundingWallCount(int gridX, int gridY)
    {
        int wallCount = 0;
        for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++)
        {
            for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++)
            {
                if (IsInMapRange(neighbourX, neighbourY))
                {
                    if (neighbourX != gridX || neighbourY != gridY)
                    {
                        wallCount += this.map[neighbourX, neighbourY];
                    }
                }
            }
        }
        return wallCount;
    }


    int[,] GenerateBorderedMap()
    {
        //border around the map
        int[,] borderedMap = new int[width + borderSize * 2, height + borderSize * 2];

        for (int x = 0; x < borderedMap.GetLength(0); x++)
        {
            for (int y = 0; y < borderedMap.GetLength(1); y++)
            {
                if (x >= borderSize && x < width + borderSize &&
                    y >= borderSize && y < height + borderSize) //in the map
                {
                    borderedMap[x, y] = this.map[x - borderSize, y - borderSize];
                }
                else borderedMap[x, y] = 1;
            }
        }
        return borderedMap;
    }


    void SmoothMap()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                int neighbourWallTiles = GetSurroundingWallCount(x, y);

                if (neighbourWallTiles > 4) this.map[x, y] = 1;
                else if (neighbourWallTiles < 4) this.map[x, y] = 0;
            }
        }
    }


    /**
    * \fn List<Coordinates> GetRegionTiles(int startX, int startY)
    * \brief get a list of coordinates
    * \return list of tiles from a region
    */
    List<Coordinates> GetRegionTiles(int startX, int startY)
    {
        List<Coordinates> tilesList = new List<Coordinates>();
        int[,] mapFlags = new int[width, height]; // which tile has been looked at
        int tileType = this.map[startX, startY];

        Queue<Coordinates> queue = new Queue<Coordinates>();
        queue.Enqueue(new Coordinates(startX, startY));
        mapFlags[startX, startY] = 1;

        while (queue.Count > 0)
        {
            Coordinates tileCoordinates = queue.Dequeue();
            tilesList.Add(tileCoordinates);

            for (int x = tileCoordinates.tileX - 1; x <= tileCoordinates.tileX + 1; x++)
            {
                for (int y = tileCoordinates.tileY - 1; y <= tileCoordinates.tileY + 1; y++)
                {
                    if (IsInMapRange(x, y) && (x == tileCoordinates.tileX || y == tileCoordinates.tileY))
                    {
                        if (mapFlags[x, y] == 0 && this.map[x, y] == tileType)
                        {
                            mapFlags[x, y] = 1;
                            queue.Enqueue(new Coordinates(x, y));
                        }
                    }
                }
            }
        }

        return tilesList;
    }


    /**
     * \fn List<List<Coordinates>> GetRegionsList(int tileType)
     * \brief get a list of region
     * \return list of regions
     */
    List<List<Coordinates>> GetRegionsList(int tileType)
    {
        List<List<Coordinates>> regionsList = new List<List<Coordinates>>();
        int[,] mapFlag = new int[width, height];

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (mapFlag[x, y] == 0 && this.map[x, y] == tileType)
                {
                    List<Coordinates> newRegion = GetRegionTiles(x, y);
                    regionsList.Add(newRegion);

                    foreach (Coordinates tileCoordinates in newRegion)
                    {
                        mapFlag[tileCoordinates.tileX, tileCoordinates.tileY] = 1;
                    }
                }
            }
        }

        return regionsList;
    }


    Vector3 CoordinatesToWorldPoint(Coordinates tileCoordinates)
    {
        return new Vector3(-width / 2 + 0.5f + tileCoordinates.tileX,
            y: 2, z: -height / 2 + 0.5f + tileCoordinates.tileY);
    }


    private void CreatePassage(Room roomA, Room roomB, Coordinates tilesA, Coordinates tilesB)
    {
        Room.ConnectRooms(roomA, roomB);
        Debug.DrawLine(CoordinatesToWorldPoint(tilesA),
            CoordinatesToWorldPoint(tilesB), Color.green, 100);
    }


    void ConnectClosestRooms(List<Room> allRooms)
    {
        int bestDistance = 0;
        Coordinates bestTilesA = new Coordinates();
        Coordinates bestTilesB = new Coordinates();
        Room bestRoomA = new Room();
        Room bestRoomB = new Room();
        bool possibleConnectionFound = false;

        foreach (Room roomA in allRooms)
        {
            possibleConnectionFound = false;

            foreach (Room roomB in allRooms)
            {
                if (roomA == roomB) continue; //skip ahead to the next roomB
                if (roomA.IsConnected(roomB))
                {
                    possibleConnectionFound = false;
                    break;
                }

                for (int tileIndexA = 0; tileIndexA < roomA.edgeTilesList.Count; tileIndexA++)
                {
                    for (int tileIndexB = 0; tileIndexB < roomB.edgeTilesList.Count; tileIndexB++)
                    {
                        Coordinates tileACoordinates = roomA.edgeTilesList[tileIndexA];
                        Coordinates tileBCoordinates = roomB.edgeTilesList[tileIndexB];
                        int distanceBetweenRooms =
                            (int) (Mathf.Pow(tileACoordinates.tileX - tileBCoordinates.tileX, 2) +
                                   Mathf.Pow(tileACoordinates.tileY - tileBCoordinates.tileY,
                                       2)); // no square root since we don't need the actual distance, just to compare them together

                        if (distanceBetweenRooms < bestDistance || !possibleConnectionFound)
                        {
                            bestDistance = distanceBetweenRooms;
                            possibleConnectionFound = true;
                            bestTilesA = tileACoordinates;
                            bestTilesB = tileBCoordinates;
                            bestRoomA = roomA;
                            bestRoomB = roomB;
                        }
                    }
                }
            }

            if (possibleConnectionFound)
            {
                CreatePassage(bestRoomA, bestRoomB, bestTilesA, bestTilesB);
            }
        }
    }


    /**
     * \fn private void ProcessMap()
     * \brief delete region with a size < threshold
     */
    private void ProcessMap()
    {
        List<List<Coordinates>> wallRegionsList = GetRegionsList(1);

        foreach (List<Coordinates> wallRegion in wallRegionsList)
        {
            if (wallRegion.Count < wallThresholdSize)
            {
                foreach (Coordinates tileCoordinates in wallRegion)
                {
                    this.map[tileCoordinates.tileX, tileCoordinates.tileY] = 0;
                }
            }
        }

        List<List<Coordinates>> roomRegionsList = GetRegionsList(0);
        List<Room> survivingRooms = new List<Room>();

        foreach (List<Coordinates> roomRegion in roomRegionsList)
        {
            if (roomRegion.Count < roomThresholdSize)
            {
                foreach (Coordinates tileCoordinates in roomRegion)
                {
                    this.map[tileCoordinates.tileX, tileCoordinates.tileY] = 1;
                }
            }
            else
            {
                survivingRooms.Add(new Room(roomRegion, this.map));
            }
        }

        ConnectClosestRooms(survivingRooms);
    }


    void GenerateMap()
    {
        this.map = new int[width, height];
        RandomFillMap();

        for (int i = 0; i < 5; i++) SmoothMap();

        ProcessMap();

        MeshGenerator meshGenerator = GetComponent<MeshGenerator>();
        meshGenerator.GenerateMesh(GenerateBorderedMap(), 1);
    }

    #endregion


    //___________________________________________________
    //___________________________________________________

    #region Unity functions

    void Start()
    {
        GenerateMap();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GenerateMap();
        }
    }

    //    void OnDrawGizmos()
    //    {
    //        if (this.map != null)
    //        {
    //            for (int x = 0; x < width; x++)
    //            {
    //                for (int y = 0; y < height; y++)
    //                {
    //                    Gizmos.color = (this.map[x, y] == 1 ? Color.black : Color.white);
    //                    Vector3 positionVector3 = new Vector3(-width / 2 + x + 0.5f, 0, -height / 2 + y + 0.5f);
    //                    Gizmos.DrawCube(positionVector3, Vector3.one);
    //                }
    //            }
    //        }
    //    }

    #endregion
}