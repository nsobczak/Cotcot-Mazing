﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshGenerator : MonoBehaviour
{
    #region Structure and class

    /**
     * \struct Triangle
     * \brief node = mid node on and edge
     */
    struct Triangle
    {
        public int vertexIndexA;
        public int vertexIndexB;
        public int vertexIndexC;

        private int[] vertices;


        public Triangle(int vertexIndexA, int vertexIndexB, int vertexIndexC)
        {
            this.vertexIndexA = vertexIndexA;
            this.vertexIndexB = vertexIndexB;
            this.vertexIndexC = vertexIndexC;

            this.vertices = new int[3];
            this.vertices[0] = vertexIndexA;
            this.vertices[1] = vertexIndexB;
            this.vertices[2] = vertexIndexC;
        }


        //indexer
        public int this[int i]
        {
            get { return this.vertices[i]; }
        }


        public bool Contains(int vertexIndex)
        {
            return (vertexIndex == vertexIndexA || vertexIndex == vertexIndexB || vertexIndex == vertexIndexC);
        }
    }


    //_____________________________________________
    /** 
     * \class Node
     * \brief node = mid node on and edge
     */
    public class Node
    {
        public Vector3 positionVector3;
        public int vertexIndex = -1;


        /*Constructor*/
        public Node(Vector3 positionVector3)
        {
            this.positionVector3 = positionVector3;
        }
    }


    //_____________________________________________
    /** 
      * \class ControlNode
      * \brief control node = corner = vertex
      */
    public class ControlNode : Node
    {
        public bool active;
        public Node aboveNode, rightNode;


        /*Constructor*/
        public ControlNode(Vector3 positionVector3, bool active, float squareSize) : base(positionVector3)
        {
            this.active = active;
            aboveNode = new Node(positionVector3 + Vector3.forward * squareSize / 2);
            rightNode = new Node(positionVector3 + Vector3.right * squareSize / 2);
        }
    }


    //_____________________________________________
    /** 
      * \class Square
      * \brief 
      */
    public class Square
    {
        public ControlNode topLeftControlNode, topRightControlNode, bottomRightControlNode, bottomLeftControlNode;
        public Node centreTopNode, centreRightNode, centreBottomNode, centreLeftNode;
        public int configuration = 0;


        /*Constructor*/
        public Square(ControlNode topLeftControlNode, ControlNode topRightControlNode,
            ControlNode bottomRightControlNode, ControlNode bottomLeftControlNode)
        {
            this.topLeftControlNode = topLeftControlNode;
            this.topRightControlNode = topRightControlNode;
            this.bottomRightControlNode = bottomRightControlNode;
            this.bottomLeftControlNode = bottomLeftControlNode;

            this.centreTopNode = topLeftControlNode.rightNode;
            this.centreRightNode = bottomRightControlNode.aboveNode;
            this.centreBottomNode = bottomLeftControlNode.rightNode;
            this.centreLeftNode = bottomLeftControlNode.aboveNode;

            if (topLeftControlNode.active) configuration += 8;
            if (topRightControlNode.active) configuration += 4;
            if (bottomRightControlNode.active) configuration += 2;
            if (bottomLeftControlNode.active) configuration += 1;
        }
    }


    //_____________________________________________
    /** 
      * \class SquareGrid
      * \brief 2d array of squares
      */
    public class SquareGrid
    {
        public Square[,] squares;


        /*Constructor*/
        public SquareGrid(int[,] map, float squareSize)
        {
            int nodeCountX = map.GetLength(0);
            int nodeCountY = map.GetLength(1);
            float mapWidth = nodeCountX * squareSize;
            float mapHeight = nodeCountY * squareSize;

            //Grid of control nodes
            ControlNode[,] controlNodes = new ControlNode[nodeCountX, nodeCountY];
            for (int x = 0; x < nodeCountX; x++)
            {
                for (int y = 0; y < nodeCountY; y++)
                {
                    //calculate position of current control node
                    Vector3 positionVector3 = new Vector3(-mapWidth / 2 + x * squareSize + squareSize / 2,
                        0, -mapHeight + y * squareSize + squareSize / 2);
                    controlNodes[x, y] = new ControlNode(positionVector3, (map[x, y] == 1), squareSize);
                }
            }

            //Grid of squares
            this.squares = new Square[nodeCountX - 1, nodeCountY - 1];
            for (int x = 0; x < nodeCountX - 1; x++)
            {
                for (int y = 0; y < nodeCountY - 1; y++)
                {
                    this.squares[x, y] = new Square(controlNodes[x, y + 1], controlNodes[x + 1, y + 1],
                        controlNodes[x + 1, y], controlNodes[x, y]);
                }
            }
        }
    }

    #endregion


    //_____________________________________________
    //_____________________________________________

    #region Attributes

    public float wallHeight;
    public SquareGrid squareGrid;
    public MeshFilter wallsMeshFilter;


    private List<Vector3> vertices;
    private List<int> triangles;

    private Dictionary<int, List<Triangle>> triangleDictionary = new Dictionary<int, List<Triangle>>();
    List<List<int>> outlinesList = new List<List<int>>();
    HashSet<int> checkedVerticesHashSet = new HashSet<int>();

    #endregion


    //_____________________________________________
    //_____________________________________________

    #region Main functions

    void AssignVertices(Node[] points)
    {
        for (int i = 0; i < points.Length; i++)
        {
            if (points[i].vertexIndex == -1) //if point not already assigned
            {
                points[i].vertexIndex = this.vertices.Count;
                this.vertices.Add(points[i].positionVector3);
            }
        }
    }


    void CreateTriangle(Node aNode, Node bNode, Node cNode)
    {
        this.triangles.Add(aNode.vertexIndex);
        this.triangles.Add(bNode.vertexIndex);
        this.triangles.Add(cNode.vertexIndex);

        Triangle triangle = new Triangle(aNode.vertexIndex, bNode.vertexIndex, cNode.vertexIndex);
        AddTriangleToDictionary(triangle.vertexIndexA, triangle);
        AddTriangleToDictionary(triangle.vertexIndexB, triangle);
        AddTriangleToDictionary(triangle.vertexIndexC, triangle);
    }


    /**
     * \fn void AddTriangleToDictionary(int vertexIndexKey, Triangle triangle)
     * \brief add triangle to triangle list of a vertex key
     */
    void AddTriangleToDictionary(int vertexIndexKey, Triangle triangle)
    {
        if (this.triangleDictionary.ContainsKey(vertexIndexKey))
        {
            this.triangleDictionary[vertexIndexKey].Add(triangle);
        }
        else
        {
            List<Triangle> triangleList = new List<Triangle>();
            triangleList.Add(triangle);
            this.triangleDictionary.Add(vertexIndexKey, triangleList);
        }
    }


    void MeshFromPoints(params Node[] points)
    {
        AssignVertices(points);

        if (points.Length >= 3) CreateTriangle(points[0], points[1], points[2]);
        if (points.Length >= 4) CreateTriangle(points[0], points[2], points[3]);
        if (points.Length >= 5) CreateTriangle(points[0], points[3], points[4]);
        if (points.Length >= 6) CreateTriangle(points[0], points[4], points[5]);
    }


    bool IsOutlineEdge(int vertexA, int vertexB)
    {
        List<Triangle> trianglesContainingVertexA = this.triangleDictionary[vertexA];
        int sharedTriangleCount = 0;

        for (int i = 0; i < trianglesContainingVertexA.Count; i++)
        {
            if (trianglesContainingVertexA[i].Contains(vertexB))
            {
                sharedTriangleCount++;
                if (sharedTriangleCount > 1) break;
            }
        }
        return sharedTriangleCount == 1;
    }


    /**
     * \fn int GetConnectedOutlineVertex(int vertexIndex)
     * \brief get the vertex that forms an outline edge with a given vertex
     * \return the vertex or -1 if there is no connected outline vertex
     */
    int GetConnectedOutlineVertex(int vertexIndex)
    {
        List<Triangle> trianglesContainingVertex = this.triangleDictionary[vertexIndex];

        for (int i = 0; i < trianglesContainingVertex.Count; i++)
        {
            Triangle triangle = trianglesContainingVertex[i];

            for (int j = 0; j < 3; j++)
            {
                int vertexB = triangle[j];

                if (vertexIndex != vertexB && !this.checkedVerticesHashSet.Contains(vertexB))
                {
                    if (IsOutlineEdge(vertexIndex, vertexB))
                        return vertexB;
                }
            }
        }
        return -1;
    }


    private void FollowOutline(int vertexIndex, int outlineIndex)
    {
        this.outlinesList[outlineIndex].Add(vertexIndex);
        this.checkedVerticesHashSet.Add(vertexIndex);
        int nextVertexIndex = GetConnectedOutlineVertex(vertexIndex);

        if (nextVertexIndex != -1)
        {
            FollowOutline(nextVertexIndex, outlineIndex);
        }
    }


    /**
     * \fn void CalculateMeshOutlines()
     * \brief look through every map vertex if it is an outline one, follow that outline, meet up with itself, add that to the outlinesList list
     */
    void CalculateMeshOutlines()
    {
        for (int vertexIndex = 0; vertexIndex < this.vertices.Count; vertexIndex++)
        {
            if (!checkedVerticesHashSet.Contains(vertexIndex))
            {
                int newOutlineVertex = GetConnectedOutlineVertex(vertexIndex);
                if (newOutlineVertex != -1) //if this is an outline vertex
                {
                    checkedVerticesHashSet.Add(vertexIndex);

                    List<int> newOutlineList = new List<int>();
                    newOutlineList.Add(vertexIndex);
                    this.outlinesList.Add(newOutlineList);
                    FollowOutline(newOutlineVertex, this.outlinesList.Count - 1);
                    this.outlinesList[this.outlinesList.Count - 1].Add(vertexIndex);
                }
            }
        }
    }


    private void TriangulateSquare(Square square)
    {
        switch (square.configuration)
        {
            case 0:
                break;

            // 1 point:
            case 1:
                MeshFromPoints(square.centreLeftNode, square.centreBottomNode, square.bottomLeftControlNode);
                break;
            case 2:
                MeshFromPoints(square.bottomRightControlNode, square.centreBottomNode, square.centreRightNode);
                break;
            case 4:
                MeshFromPoints(square.topRightControlNode, square.centreRightNode, square.centreTopNode);
                break;
            case 8:
                MeshFromPoints(square.topLeftControlNode, square.centreTopNode, square.centreLeftNode);
                break;

            // 2 points:
            case 3:
                MeshFromPoints(square.centreRightNode, square.bottomRightControlNode, square.bottomLeftControlNode,
                    square.centreLeftNode);
                break;
            case 6:
                MeshFromPoints(square.centreTopNode, square.topRightControlNode, square.bottomRightControlNode,
                    square.centreBottomNode);
                break;
            case 9:
                MeshFromPoints(square.topLeftControlNode, square.centreTopNode, square.centreBottomNode,
                    square.bottomLeftControlNode);
                break;
            case 12:
                MeshFromPoints(square.topLeftControlNode, square.topRightControlNode, square.centreRightNode,
                    square.centreLeftNode);
                break;
            case 5:
                MeshFromPoints(square.centreTopNode, square.topRightControlNode, square.centreRightNode,
                    square.centreBottomNode, square.bottomLeftControlNode, square.centreLeftNode);
                break;
            case 10:
                MeshFromPoints(square.topLeftControlNode, square.centreTopNode, square.centreRightNode,
                    square.bottomRightControlNode, square.centreBottomNode, square.centreLeftNode);
                break;

            // 3 point:
            case 7:
                MeshFromPoints(square.centreTopNode, square.topRightControlNode, square.bottomRightControlNode,
                    square.bottomLeftControlNode, square.centreLeftNode);
                break;
            case 11:
                MeshFromPoints(square.topLeftControlNode, square.centreTopNode, square.centreRightNode,
                    square.bottomRightControlNode, square.bottomLeftControlNode);
                break;
            case 13:
                MeshFromPoints(square.topLeftControlNode, square.topRightControlNode, square.centreRightNode,
                    square.centreBottomNode, square.bottomLeftControlNode);
                break;
            case 14:
                MeshFromPoints(square.topLeftControlNode, square.topRightControlNode, square.bottomRightControlNode,
                    square.centreBottomNode, square.centreLeftNode);
                break;

            // 4 point:
            case 15:
                MeshFromPoints(square.topLeftControlNode, square.topRightControlNode, square.bottomRightControlNode,
                    square.bottomLeftControlNode);
                //None of these verteces can be outline edges
                this.checkedVerticesHashSet.Add(square.topLeftControlNode.vertexIndex);
                this.checkedVerticesHashSet.Add(square.topRightControlNode.vertexIndex);
                this.checkedVerticesHashSet.Add(square.bottomRightControlNode.vertexIndex);
                this.checkedVerticesHashSet.Add(square.bottomLeftControlNode.vertexIndex);
                break;
        }
    }


    private void CreateWallMesh()
    {
        CalculateMeshOutlines();

        List<Vector3> wallVerticesList = new List<Vector3>();
        List<int> wallTriangleList = new List<int>();
        Mesh wallMesh = new Mesh();

        foreach (List<int> outline in this.outlinesList)
        {
            for (int i = 0; i < outline.Count - 1; i++) // for each vertexj
            {
                int startIndex = wallVerticesList.Count;
                //Add 4 vertices to make a wall
                wallVerticesList.Add(this.vertices[outline[i]]); //left
                wallVerticesList.Add(this.vertices[outline[i + 1]]); //right
                wallVerticesList.Add(this.vertices[outline[i]] - Vector3.up * wallHeight); //bottom left
                wallVerticesList.Add(this.vertices[outline[i + 1]] - Vector3.up * wallHeight); //bottom right

                //anti clockwise
                wallTriangleList.Add(startIndex + 0); //top left
                wallTriangleList.Add(startIndex + 2); //bottom left
                wallTriangleList.Add(startIndex + 3); //bottom right

                wallTriangleList.Add(startIndex + 3); //bottom right
                wallTriangleList.Add(startIndex + 1); //top right
                wallTriangleList.Add(startIndex + 0); //top left
            }
        }
        wallMesh.vertices = wallVerticesList.ToArray();
        wallMesh.triangles = wallTriangleList.ToArray();
        wallsMeshFilter.mesh = wallMesh;
    }


    public void GenerateMesh(int[,] map, float squareSize)
    {
        this.triangleDictionary.Clear();
        this.outlinesList.Clear();
        this.checkedVerticesHashSet.Clear();

        this.squareGrid = new SquareGrid(map, squareSize);

        this.vertices = new List<Vector3>();
        this.triangles = new List<int>();

        for (int x = 0; x < this.squareGrid.squares.GetLength(0); x++)
        {
            for (int y = 0; y < this.squareGrid.squares.GetLength(1) - 1; y++)
            {
                TriangulateSquare(this.squareGrid.squares[x, y]);
            }
        }

        Mesh mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;

        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.RecalculateNormals();

        CreateWallMesh();
    }

    #endregion


    //_____________________________________________
    //_____________________________________________

    #region Unity functions

    /**
     * \fn public void OnDrawGizmos()
     * \brief draw nodes and control nodes
     */
    public void OnDrawGizmos()
    {
        if (this.squareGrid != null)
        {
            for (int x = 0; x < this.squareGrid.squares.GetLength(0); x++)
            {
                for (int y = 0; y < this.squareGrid.squares.GetLength(1) - 1; y++)
                {
                    Gizmos.color = (this.squareGrid.squares[x, y].topLeftControlNode.active
                        ? Color.black
                        : Color.white);
                    Gizmos.DrawCube(this.squareGrid.squares[x, y].topLeftControlNode.positionVector3,
                        Vector3.one * 0.4f);

                    Gizmos.color = (this.squareGrid.squares[x, y].topRightControlNode.active
                        ? Color.black
                        : Color.white);
                    Gizmos.DrawCube(this.squareGrid.squares[x, y].topRightControlNode.positionVector3,
                        Vector3.one * 0.4f);

                    Gizmos.color = (this.squareGrid.squares[x, y].bottomRightControlNode.active
                        ? Color.black
                        : Color.white);
                    Gizmos.DrawCube(this.squareGrid.squares[x, y].bottomRightControlNode.positionVector3,
                        Vector3.one * 0.4f);

                    Gizmos.color = (this.squareGrid.squares[x, y].bottomLeftControlNode.active
                        ? Color.black
                        : Color.white);
                    Gizmos.DrawCube(this.squareGrid.squares[x, y].bottomLeftControlNode.positionVector3,
                        Vector3.one * 0.4f);

                    Gizmos.color = Color.grey;
                    Gizmos.DrawCube(this.squareGrid.squares[x, y].centreTopNode.positionVector3,
                        Vector3.one * 0.15f);
                    Gizmos.DrawCube(this.squareGrid.squares[x, y].centreRightNode.positionVector3,
                        Vector3.one * 0.15f);
                    Gizmos.DrawCube(this.squareGrid.squares[x, y].centreBottomNode.positionVector3,
                        Vector3.one * 0.15f);
                    Gizmos.DrawCube(this.squareGrid.squares[x, y].centreLeftNode.positionVector3,
                        Vector3.one * 0.15f);
                }
            }
        }
    }

    #endregion
}